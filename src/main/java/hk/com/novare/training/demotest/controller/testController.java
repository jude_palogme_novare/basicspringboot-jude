package hk.com.novare.training.demotest.controller;

import hk.com.novare.training.demotest.entity.UserEntity;
import hk.com.novare.training.demotest.service.testService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
public class testController {

    private testService ts;

    @Autowired
    public testController(testService ts){
        this.ts = ts;
    }

    @PostMapping("/email")
    public UserEntity sender(@RequestBody UserEntity u) {
        ts.sendEmail(u);
        return u;
    }
}
