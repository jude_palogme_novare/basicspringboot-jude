package hk.com.novare.training.demotest.service;

import hk.com.novare.training.demotest.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
public class testServiceImpl implements testService {

    private MailSender ms;

    @Autowired
    public void setMailSender(MailSender ms) {
        this.ms = ms;
    }

    public void sendEmail(UserEntity u) {
        System.out.println("test");
        SimpleMailMessage s = new SimpleMailMessage();

        s.setTo(u.getEmail());
        s.setSubject(u.getMailSubject());
        s.setText(u.getMailBody());

        ms.send(s);
    }
}
